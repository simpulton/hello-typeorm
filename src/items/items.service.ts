import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Item } from './item.entity';

@Injectable()
export class ItemsService {
  constructor(
    @InjectRepository(Item)
    private readonly itemRepository: Repository<Item>,
  ) {}

  async all(): Promise<Item[]> {
    return await this.itemRepository.find();
  }

  async find(id: string): Promise<Item> {
    return await this.itemRepository.findOne(id);
  }

  async create(item: Item): Promise<Item> {
    return await this.itemRepository.create(item);
  }

  async update(id: string, item: Item): Promise<UpdateResult> {
    return await this.itemRepository.update(id, item);
  }

  async delete(id: string): Promise<DeleteResult> {
    return await this.itemRepository.delete(id);
  }
}
