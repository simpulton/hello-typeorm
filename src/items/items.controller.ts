import { Controller, Get, Param, Post, Body, Put, Delete } from '@nestjs/common';
import { ItemsService } from './items.service';
import { Item } from './item.entity';
import { UpdateResult, DeleteResult } from 'typeorm';

@Controller('items')
export class ItemsController {

  constructor(private itemsService: ItemsService) {}
   
  @Get()
  all(): Promise<Item[]> {
    return this.itemsService.all();
  }

  @Get(':id')
  find(@Param('id') id: string): Promise<Item> {
    return this.itemsService.find(id);
  }

  @Post()
  create(@Body() item: Item): Promise<Item> {
    return this.itemsService.create(item);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() item: Item): Promise<UpdateResult> {
    return this.itemsService.update(id, item);
  }

  @Delete(':id')
  delete(@Param('id') id: string): Promise<DeleteResult> {
    return this.itemsService.delete(id);
  }
}
