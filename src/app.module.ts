import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ItemsModule } from './items/items.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'ec2-54-243-197-120.compute-1.amazonaws.com',
      port: 5432,
      username: 'iujlhbykqevwle',
      password: '7678440660dd823f07def78188f64eb5f1d5d85497abc4652db57294a518344b',
      database: 'd5er4l6a5q6bh2',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    ItemsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
